package com.inneex.www.customfonts;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.widget.Button;

import com.inneex.www.customfonts.utilities.CustomFontHelper;

/**
 * Created by Jimit Patel on 24/07/15.
 * A custom Button for supporting different fonts kept in asset folder.
 * To implement this also use declare-styleable for set of custom attributes
 */
public class FontButton extends Button {

    public FontButton(Context context) {
        super(context);
    }

    public FontButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        CustomFontHelper.setCustomFont(this, context, attrs);
    }

    public FontButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        CustomFontHelper.setCustomFont(this, context, attrs);
    }

    public void setFont(@NonNull String font) {
        CustomFontHelper.setCustomFont(this, font, getContext());
    }
}
