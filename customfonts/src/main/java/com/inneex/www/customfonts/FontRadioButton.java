package com.inneex.www.customfonts;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.widget.RadioButton;

import com.inneex.www.customfonts.utilities.CustomFontHelper;

/**
 * Created by rohitjain on 04/08/15.
 */
public class FontRadioButton extends RadioButton {


    public FontRadioButton(Context context) {
        super(context);
    }

    public FontRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        CustomFontHelper.setCustomFont(this, context, attrs);
    }

    public FontRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        CustomFontHelper.setCustomFont(this, context, attrs);
    }

    public void setFont(@NonNull String font) {
        CustomFontHelper.setCustomFont(this, font, getContext());
    }
}
