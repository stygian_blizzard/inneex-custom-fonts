package com.inneex.www.customfonts;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.widget.CheckBox;

import com.inneex.www.customfonts.utilities.CustomFontHelper;

/**
 * Created by Jimit Patel on 14/08/15.
 */
public class FontCheckbox extends CheckBox {


    public FontCheckbox(Context context) {
        super(context);
    }

    public FontCheckbox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        CustomFontHelper.setCustomFont(this, context, attrs);
    }

    public FontCheckbox(Context context, AttributeSet attrs) {
        super(context, attrs);
        CustomFontHelper.setCustomFont(this, context, attrs);
    }

    public void setFont(@NonNull String font) {
        CustomFontHelper.setCustomFont(this, font, getContext());
    }
}

