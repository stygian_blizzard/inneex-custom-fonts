package com.inneex.www.customfonts;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.widget.EditText;

import com.inneex.www.customfonts.utilities.CustomFontHelper;

/**
 * Created by Jimit Patel on 29/07/15.
 */
public class FontEditText extends EditText {
    public FontEditText(Context context) {
        super(context);
    }

    public FontEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        CustomFontHelper.setCustomFont(this, context, attrs);
    }

    public FontEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        CustomFontHelper.setCustomFont(this, context, attrs);
    }

    public void setFont(@NonNull String font) {
        CustomFontHelper.setCustomFont(this, font, getContext());
    }
}
